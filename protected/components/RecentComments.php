<?php

Yii::import('zii.widgets.CPortlet');

class RecentComments extends CWidget
{
	public $title;
	public $maxComments=10;

	public function getRecentComments()
	{
		return Comment::model()->findRecentComments($this->maxComments);
	}

	public function init()
	{
		$this->title=CHtml::encode(Yii::t('blog','Recent Comments'));
		parent::init();
    }

    public function run()
	{
		$this->render('recentComments');
	}
}
