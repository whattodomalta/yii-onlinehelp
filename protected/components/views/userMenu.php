<div class="well" style="max-width: 340px; padding: 8px 0;">
<?php 
$pendingCommentCount = Comment::model()->pendingCommentCount;
$this->widget('bootstrap.widgets.TbMenu', array(
    'type'=>'list',
    'items'=>array(
        array('label'=>$title),
        array('label'=>Yii::t('blog','Create New Post'), 'icon'=>'star', 'url'=>array('/post/create'), 'visible'=>Yii::app()->controller->id!=='post'),
        array('label'=>Yii::t('blog','Manage Posts'), 'icon'=>'th-list', 'url'=>array('/post/admin'), 'visible'=>Yii::app()->controller->id!=='post' and Yii::app()->controller->action->id!=='admin'),
        array('label'=>Yii::t('blog','Approve Comments'). ' (' . $pendingCommentCount . ')', 'icon'=>'ok', 'url'=>array('/comment/index'), 'visible'=>$pendingCommentCount>0),
        array('label'=>Yii::t('site','Logout'), 'icon'=>'share', 'url'=>array('/site/logout'), 'visible'=>!Yii::app()->user->isGuest),
    ),
)); ?>
</div>
