<div class="well" style="max-width: 340px; padding: 8px 0;">
<?php $comments = $this->getRecentComments(); ?>
<ul class="nav nav-list">
    <li class="nav-header"><?php echo CHtml::encode($this->title); ?></li>
    <?php if(count($comments)==0) : ?>
    <li><?php echo CHtml::encode(Yii::t('site','No comments yet.')); ?></li>
    <?php endif; ?>
	<?php foreach($comments as $comment): ?>
	<li><?php echo $comment->authorLink; ?> on
		<?php echo CHtml::link(CHtml::encode($comment->post->title), $comment->getUrl()); ?>
	</li>
    <?php endforeach; ?>
</ul>
</div>
