<?php
/**
 * The following variables are available in this template:
 * - $this: the BootCrudCode object
 */
?>
<?php
echo "<?php\n";
$label=$this->pluralize($this->class2name($this->modelClass));
echo "\$this->breadcrumbs=array(
	Yii::t('site','$label')=>array('index'),
	Yii::t('site','Create'),
);\n";
?>

$this->menu=array(
	array('label'=>Yii::t('site','List <?php echo $label; ?>'),'url'=>array('index'), 'icon'=>'th-large'),
	array('label'=>Yii::t('site','Manage <?php echo $label; ?>'),'url'=>array('admin'), 'icon'=>'th-list'),
);
?>

<h1><?php echo "<?php echo CHtml::encode(Yii::t('site','Create a New ".$this->class2name($this->modelClass)."')); ?>"; ?></h1>

<?php echo "<?php echo \$this->renderPartial('_form', array('model'=>\$model)); ?>"; ?>
