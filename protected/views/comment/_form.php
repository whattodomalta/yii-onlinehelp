<div class="form">

<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm', array(
    'id'=>'comment-form',
    'enableAjaxValidation'=>true,
    'type'=>'horizontal',
    'htmlOptions'=>array('class'=>'well'),
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->textFieldRow($model,'author',array('class'=>'span4','maxlength'=>128)); ?>

	<?php echo $form->textFieldRow($model,'email',array('class'=>'span4','maxlength'=>128)); ?>

	<?php echo $form->textFieldRow($model,'url',array('class'=>'span4','maxlength'=>128)); ?>

	<?php echo $form->textAreaRow($model,'content',array('rows'=>4, 'class'=>'span4')); ?>

    <div class="form-actions">
        <?php $this->widget('bootstrap.widgets.TbButton', array('buttonType'=>'submit', 'type'=>'primary', 'icon'=>'ok','label'=>$model->isNewRecord ? Yii::t('blog','Submit') : Yii::t('blog','Save'))); ?>
        <?php $this->widget('bootstrap.widgets.TbButton', array('buttonType'=>'reset', 'icon'=>'repeat','label'=>'Reset')); ?>
    </div>

<?php $this->endWidget(); ?>

</div><!-- form -->
