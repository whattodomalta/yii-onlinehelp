<?php
$this->pageTitle=Yii::app()->name . ' - About';
$this->breadcrumbs=array(
	'About',
);
?>

<h1>About <em><?php echo CHtml::encode(Yii::app()->name); ?></em></h1>

<p>This is the "about" page for a blog system using <a href="www.yiiframework.com">Yii Framework</a>.</p>

<p>The blog system serves for demonstrating the <a href="http://www.yiiframework.com/extensions/yii-onlinehelp">yii-onlinehelp extension</a>.</p>
