<?php
$this->breadcrumbs=array(
    // Module Yii::t('onlinehelp','MODULE_NAME')=>array('/MODULE_ID'),
    Yii::t('onlinehelp','Online Help Pages')=>array('admin'),
    $model->title,
);

$this->menu=array(
    array('label'=>Yii::t('onlinehelp','Update Online Help Page'),'url'=>array('update','id'=>$model->id), 'icon'=>'pencil'),
    array('label'=>Yii::t('onlinehelp','Delete Online Help Page'),'url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?'), 'icon'=>'remove'),
    array('label'=>Yii::t('onlinehelp','Create New Online Help Page'),'url'=>array('create'), 'icon'=>'star'),
    array('label'=>Yii::t('onlinehelp','List Online Help Pages'),'url'=>array('index'), 'icon'=>'th-large'),
    array('label'=>Yii::t('onlinehelp','Manage Online Help Pages'),'url'=>array('admin'), 'icon'=>'th-list'),
);
$parser = new CMarkdownParser;
$content = $parser->safeTransform($model->content);
?>

<h1><?php echo CHtml::encode(Yii::t('onlinehelp','View Online Help Page {recordName}',array('{recordName}'=>$model->recordName))); ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
    'data'=>$model,
    'attributes'=>array(
        'id',
        'title',
        array(
            'name'=>'requestId',
            'type'=>'raw',
            'value'=>CHtml::link($model->requestId,array('/'.$model->requestId)),
        ),
        array(
            'name'=>'content',
            'type'=>'raw',
            'value'=>$content,
        ),
    ),
)); ?>

<?php 
/*
$this->widget('application.components.TimestampWidget',array(
    'model'=>$model,
    'title'=>Yii::t('onlinehelp','Change Log'),
));
 */
?>
