<?php
$this->breadcrumbs=array(
    // Module Yii::t('onlinehelp','MODULE_NAME')=>array('/MODULE_ID'),
    Yii::t('onlinehelp','Online Help Pages')=>array('index'),
    Yii::t('onlinehelp','Manage'),
);

$this->menu=array(
    array('label'=>Yii::t('onlinehelp','Create New Online Help Page'),'url'=>array('create'), 'icon'=>'star'),
    array('label'=>Yii::t('onlinehelp','List Online Help Pages'),'url'=>array('index'), 'icon'=>'th-large'),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
    $('.search-form').toggle();
    return false;
});
$('.search-form form').submit(function(){
    $.fn.yiiGridView.update('online-help-grid', {
        data: $(this).serialize()
    });
    return false;
});
");
?>

<h1><?php echo CHtml::encode(Yii::t('onlinehelp','Manage Online Help Pages')); ?></h1>

<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo CHtml::link(Yii::t('onlinehelp','Advanced Search'),'#',array('class'=>'search-button btn')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
    'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('bootstrap.widgets.TbGridView',array(
    'id'=>'online-help-grid',
    'dataProvider'=>$model->search(),
    'filter'=>$model,
    'columns'=>array(
        array(
            'class'=>'bootstrap.widgets.TbButtonColumn',
            'template'=>'{view}&nbsp;&nbsp;{update}',
        ),
        // 'id',
        'requestId',
        'title',
        array(
            'name'=>'isPageHelp',
            'type'=>'boolean',
            'htmlOptions'=>array('style'=>'text-align:center'),
        ),
        array(
            'name'=>'sequenceNbr',
            'htmlOptions'=>array('style'=>'text-align:center'),
        ),
        array(
            'name'=>'elementId',
            'value'=>'$data->elementId!=="" ? $data->elementId : "(n.a.)"',
        ),
        /*
        array(
            'name'=>'tsCreated',
            'type'=>'raw',
            'value'=>'Chtml::encode(Yii::app()->dateFormatter->formatDateTime($data->tsCreated,"medium","short"))',
        ),
        array(
            'name'=>'userIdCreated',
            'value'=>'$data->userCreated->recordName',
        ),
        array(
            'name'=>'tsUpdated',
            'type'=>'raw',
            'value'=>'$data->tsUpdated==0 ? "-" : Chtml::encode(Yii::app()->dateFormatter->formatDateTime($data->tsUpdated,"medium","short"))',
        ),
        array(
            'name'=>'userIdUpdated',
            'value'=>'$data->userIdUpdated==0 ? "-" : $data->userUpdated->recordName',
        ),
         */
        array(
            'class'=>'bootstrap.widgets.TbButtonColumn',
            'template'=>'{delete}',
        ),
    ),
)); ?>
