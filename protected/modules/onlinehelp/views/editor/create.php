<?php
$this->breadcrumbs=array(
    // Module Yii::t('onlinehelp','MODULE_NAME')=>array('/MODULE_ID'),
	Yii::t('onlinehelp','Online Help Pages')=>array('admin'),
	Yii::t('onlinehelp','Create'),
);

$this->menu=array(
    array('label'=>Yii::t('onlinehelp','List Online Help Pages'),'url'=>array('index'), 'icon'=>'th-large'),
	array('label'=>Yii::t('onlinehelp','Manage Online Help Pages'),'url'=>array('admin'), 'icon'=>'th-list'),
);
?>

<h1><?php echo CHtml::encode(Yii::t('onlinehelp','Create a New Online Help Page')); ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
