<?php
Yii::import('onlinehelp.models.*');
class OnlineHelpWidget extends CWidget
{
    // {{{ *** Members ***
    public $pathInfo;
    /**
     * @var MyModel ActiveRecord instance
     */
    public $model;
    public $models;
    public $showPopupGuiders;
    /**
     * @var array HTML attributes for the menu's root container tag
     */
    public $htmlOptions=array();
    public $userIsEditor = false;

    private $_assetsUrl;
    
    // }}} 
    // {{{ *** Methods ***
    // {{{ init
    public function init()
    {
        // Check if user is online help editor
        $this->userIsEditor = false;
        $users = Yii::app()->modules['onlinehelp']['users'];
        if(is_null($users)) {
            $module = get_class_vars ( 'OnlinehelpModule' );
            $users = $module['users'];
        }
        if( in_array(Yii::app()->user->getName(), $users))
            $this->userIsEditor = true;

        if(is_null($this->pathInfo))
            $this->pathInfo = Yii::app()->request->pathInfo;
        $this->htmlOptions['id']=$this->getId();
        $model = OnlineHelp::model()->findByAttributes(array('requestId'=>$this->pathInfo, 'isPageHelp'=>1));
        if(is_null($model))
            $model = null;
        $this->model = $model;

        $criteria = new CDbCriteria;
        $criteria->compare('requestId', $this->pathInfo);
        $criteria->compare('isPageHelp', 0);
        $criteria->order = 'sequenceNbr';
        $models = OnlineHelp::model()->findAll($criteria); //ByAttributes(array('requestId'=>$this->pathInfo, 'isPageHelp'=>0), array('order'=>'sequenceNbr'));
        if(count($models)==0)
            $models = null;
        $this->models = $models;

        $this->registerCss();

    } // }}} 
    // {{{ run
    public function run()
    {
        // Hide popups for user/request?
        $this->showPopupGuiders = false;
        $sql = "SELECT COUNT(*) FROM `{{useronlinehelptohide}}` WHERE userId=:userId AND requestId=:requestId";
        $result = Yii::app()->db->createCommand($sql)->queryScalar( array(':userId'=>Yii::app()->user->id, ':requestId'=>$this->pathInfo) );
        if($result==0)
            $this->showPopupGuiders = true;
        $template = 'onlineHelpWidget';
        $this->render($template,array( 'userIsEditor'=>$this->userIsEditor, 'showPopupGuiders'=>$this->showPopupGuiders, 'requestId'=>$this->pathInfo, 'model'=>$this->model, 'models'=>$this->models));
    } // }}} 
	/**
	 * Registers the module CSS.
	 */
	public function registerCss()
	{
		Yii::app()->clientScript->registerScriptFile($this->getAssetsUrl() . '/js/shortcut.js');
		Yii::app()->clientScript->registerCssFile($this->getAssetsUrl() . '/css/onlinehelp.css');
    }
    // {{{ getAssetsUrl
	/**
	 * Returns the URL to the published assets folder.
	 * @return string the URL.
	 */
	protected function getAssetsUrl()
	{
        if (isset($this->_assetsUrl))
        {
            Yii::log('assetsUrl: '.$this->_assetsUrl);
			return $this->_assetsUrl;
        }
        else
		{
			$assetsPath = Yii::getPathOfAlias('onlinehelp.assets');
			$assetsUrl = Yii::app()->assetManager->publish($assetsPath); //, false, -1, $this->forceCopyAssets);
            Yii::log('assetsUrl: '.$assetsUrl);

			return $this->_assetsUrl = $assetsUrl;
		}
    } // }}} 
    // }}} End Methods
}
/* vim:set ai sw=4 sts=4 et fdm=marker fdc=4: */ 
?>
